USE classicmodels2;

ALTER TABLE customers
MODIFY country char(3) NOT NULL DEFAULT '';
#ADD FOREIGN KEY (country) REFERENCES world.country(Code);

ALTER TABLE offices
MODIFY country char(3) NOT NULL DEFAULT '';
#ADD FOREIGN KEY (country) REFERENCES world.country(Code);

ALTER TABLE orders
MODIFY status ENUM ("Shipped", "on Hold","Disputed", "In Process", "Resolved", "Cancelled") NOT NULL DEFAULT "In Process";

ALTER TABLE customers
ADD language char(2) NOT NULL;




-- UPDATE classicmodel.customers AS c
-- JOIN world.country AS w ON c.country = w.Name
-- SET c.country = w.Code;

-- SELECT CountryCode, Language
-- FROM world.countrylanguage
-- WHERE Percentage = (SELECT MAX(Percentage) FROM world.countrylanguage WHERE CountryCode = cl.CountryCode);

-- UPDATE classicmodel.customer cl
-- JOIN (
--     SELECT CountryCode, Language
--     FROM world.countrylanguage
--     WHERE Percentage = (SELECT MAX(Percentage) FROM world.countrylanguage WHERE CountryCode = cl.CountryCode)
-- ) AS lang_info ON cl.country = lang_info.CountryCode
-- SET cl.language = lang_info.Language;
