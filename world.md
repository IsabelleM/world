# Réponses aux questions

## 1. Liste des types de gouvernement avec nombre de pays pour chaque.

```SQL
SELECT count(GovernmentForm) AS Nbrepays, GovernmentForm FROM country 
GROUP BY GovernmentForm ORDER BY Nbrepays DESC;
```
![Réponse question 1](/Question1.png)

## 2.Pourquoi countrylanguage.IsOfficial utilise un enum et pas un bool ?

Les types énumérés (enum) sont des types de données qui comprennent un ensemble statique, prédéfini de valeurs dans un ordre spécifique. Cela permet de choisir entre plusieurs valeurs, ici si la langue parlée dans le pays est la langue officielle ou non.
Et à l'époque de création de la base de données, il semblerait que les booléens n'existaient pas encore.

## 3. D’apres la BDD, combien de personnes dans le monde parlent anglais ?

```SQL
SELECT language, SUM((percentage * population)/100) FROM countrylanguage 
JOIN country ON country.code = countrylanguage.countryCode WHERE language = "English";
```
![Réponse question 3](/Question3.png)

## 4. Faire la liste des langues avec le nombre de locuteurs, de la plus parlée à la moins parlée.
```SQL
SELECT Language, SUM((Percentage * Population)/100) AS Nbrelocuteurs FROM countrylanguage 
JOIN country ON country.Code = countrylanguage.CountryCode GROUP BY language ORDER BY Nbrelocuteurs DESC;
```
![Réponse question 4](Question4.png)

## 5. En quelle unité est exprimée la surface des pays ?
La surface des pays est rédigée en kilomètres carrés. En effet, pour la France, on trouve pour la SurfaceArea 551500.00, ce qui correspond à la surface en kilomètres carrés.
```SQL
SELECT SurfaceArea FROM country WHERE Name = "France";
```

## 6. Faire la liste des pays qui ont plus de 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.
```SQL
SELECT country.Name, city.Name, city.Population,(city.Population / country.Population) * 100 AS PercentageInCapital FROM country JOIN city ON city.ID = country.Capital 
WHERE country.Population > "10 000 000" ORDER BY country.Population DESC;
```
![Réponse question 6](Question6.png)


## 7. Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le pourcentage de croissance.
```SQL
SELECT country.Name, (GNP/GNPOld-1)*100 AS pourcentage_croissance FROM country 
ORDER BY pourcentage_croissance DESC LIMIT 10;
```
![Réponse question 7](Question7.png)

## 8. Liste des pays plurilingues avec pour chacun le nombre de langues parlées.
```SQL
SELECT country.Name, COUNT(Language) AS Nbrelangues FROM country 
JOIN countrylanguage ON countrylanguage.CountryCode = country.Code 
GROUP BY Name HAVING COUNT(countrylanguage.Language) > 1;
```

![Réponse question 8](Question8.png)

## 9. Liste des pays avec plusieurs langues officielles, le nombre de langues officielles et le nombre de langues du pays.
```SQL
SELECT name, Nbrelangues, NbrelanguesOfficielles
FROM (
    SELECT 
    country.Name as Name, 
    COUNT(Language) AS Nbrelangues,
    country.Code as Code
    FROM country 
    JOIN countrylanguage ON country.Code = CountryCode
    GROUP BY Code
) as R1
JOIN (
    SELECT COUNT(r2.Language) AS NbrelanguesOfficielles,
    country.Code as Code
    FROM country 
    JOIN countrylanguage AS r2 ON country.Code = r2.CountryCode
    where r2.IsOfficial = 'T'
    GROUP BY Code
) as R2 
ON R1.Code = R2.Code;
```

```SQL
SELECT 
    country.Name,
    COUNT(DISTINCT Language) AS Nbrelangues,
    COUNT(DISTINCT CASE WHEN IsOfficial = 'T' THEN Language END) AS NbrelanguesOfficielles
FROM 
    country 
JOIN 
    countrylanguage ON country.Code = countrylanguage.CountryCode
GROUP BY 
    Code
HAVING 
    NbrelanguesOfficielles > 1;
```

![Réponse question 9](Question9.png)

## 10. Liste des langues parlées en France avec le % pour chacun.
```SQL
SELECT Language, Percentage FROM countrylanguage WHERE CountryCode = "FRA";
```
![Réponse question 10](Question10.png)

## 11. Pareil en Chine.
```SQL
SELECT * FROM countrylanguage WHERE CountryCode = "CHN";
```
![Réponse question 11](Question11.png)

## 12. Pareil aux Etats-Unis.
```SQL
SELECT * FROM countrylanguage WHERE CountryCode = "USA";
```
![Réponse question 12](Question12.png)

## 13. Pareil en UK.
```SQL
SELECT * FROM countrylanguage WHERE CountryCode = "GBR";
```
![Réponse question 13](Question13.png)


## 14. Pour chaque région, quelle est la langue la plus parlée et quel pourcentage de la population la parle.
```SQL
SELECT Region, Language, MAX(Percentage) FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode
GROUP BY country.Region;
```


## 15. Est-ce que la somme des pourcentages de langues parlées dans un pays est égale à 100 ? Pourquoi ?
Non. Pour les Etats-Unis, la somme est égale à 97,9; pour le Royaume-Uni, la somme des pourcentages est égale à 98,3; pour la Chine, le pourcentage total est égal à 98,7 et pour la France, la somme est égale à 98,5%. On peut supposer qu'il y a d'autres minorités non prises en compte, ou alors cela vient du fait que ce ne sont pas les langues officielles, donc il est difficile de les comptabiliser.
```SQL
SELECT Name, SUM(Percentage) AS `somme des pourcentages des langues parlées`
FROM country 
JOIN countrylanguage ON countrylanguage.CountryCode = country.Code 
GROUP BY country.Name
ORDER BY `somme des pourcentages des langues parlées`;
```

## 16. Liste des langues parlées dans le monde avec le % pour chacun.
```SQL
SELECT Language, SUM(Percentage)/100 FROM countrylanguage GROUP BY Language ORDER BY Language;
```
![Réponse question 16](Question16.png)

## 17. Faire une carte du monde, avec une couleur par région (chaque pays étant dans la bonne couleur).
```SQL
SELECT Code, Region, Name FROM country ORDER BY Region ASC;
```
cf.fichier map.py

![Réponse question 17](Question17.png)