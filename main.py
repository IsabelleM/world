import mysql.connector
import pandas as pd

connection = mysql.connector.connect(
    host="localhost",           #l'url de la base de données
    user="david",               #le nom d'utilisateur mysql
    password="coucou123",       #le mot de passe de l'utilisateur mysql
)

#Créer un curseur pour exécuter des commandes SQL
cursor = connection.cursor(dictionary=True, buffered=True)

#Insérer la table productlines
cursor.execute("""SELECT * FROM classicmodels.productlines;""")
rows = cursor.fetchall()
for row in rows:
    try:
        cursor.execute("""Insert into classicmodels2.productlines(productLine,textDescription,htmlDescription,image) values (%s, %s, %s, %s);""",[row['productLine'], row['textDescription'], row['htmlDescription'], row['image']])
    except:
        continue
connection.commit()

#Insérer la table products
cursor.execute("""SELECT * FROM classicmodels.products;""")
rows_products = cursor.fetchall()
for row in rows_products:
    cursor.execute("""Insert into classicmodels2.products(productCode,productName,productLine,productScale,productVendor,productDescription,quantityInStock,buyPrice,MSRP) values (%s,%s,%s,%s,%s,%s,%s,%s,%s);""",[row['productCode'],row['productName'],row['productLine'],row['productScale'],row['productVendor'],row['productDescription'],row['quantityInStock'],row['buyPrice'],row['MSRP']])
connection.commit()

#Insérer la table offices
cursor.execute("""UPDATE classicmodels.offices SET country = "United Kingdom" WHERE country = "UK";""")
connection.commit()
cursor.execute("""UPDATE classicmodels.offices SET country = "United States" WHERE country = "USA";""")
connection.commit()
cursor.execute("""SELECT * FROM classicmodels.offices;""")
rows_offices = cursor.fetchall()

for row in rows_offices:
    # récupérer le code du pays
    cursor.execute("""SELECT Code FROM world.country WHERE Name = %s;""", [row["country"]])
    country_code = cursor.fetchone()
    # country_code['Code']
    try:
        cursor.execute("""Insert into classicmodels2.offices(officeCode,city,phone,addressLine1,addressLine2,state,country,postalCode,territory) values (%s,%s,%s,%s,%s,%s,%s,%s,%s);""", [row["officeCode"], row["city"], row["phone"], row["addressLine1"], row["addressLine2"], row["state"], country_code['Code'], row["postalCode"], row["territory"]])
    except Exception as err:
        print(err)
        continue
connection.commit()

#Insérer la table employees
cursor.execute("""SELECT * FROM classicmodels.employees;""")
rows_employees = cursor.fetchall()
for row in rows_employees:
    try:
        cursor.execute("""Insert into classicmodels2.employees(employeeNumber,lastName,firstName,extension,email,officeCode,reportsTo,jobTitle) values (%s,%s,%s,%s,%s,%s,%s,%s);""", [row["employeeNumber"], row["lastName"], row["firstName"], row["extension"], row["email"], row["officeCode"], row["reportsTo"], row["jobTitle"]])
    except Exception as er:
        print(er)
        continue
connection.commit()


#Insérer la table customers
cursor.execute("""SELECT * FROM classicmodels.customers;""")
rows_customers = cursor.fetchall()
cursor.execute("""UPDATE classicmodels.customers SET country = "United Kingdom" WHERE country = "UK";""")
connection.commit()
cursor.execute("""UPDATE classicmodels.customers SET country = "United States" WHERE country = "USA";""")
connection.commit()
cursor.execute("""UPDATE classicmodels.customers SET Country = "Russian Federation" WHERE country = "Russia";""")
connection.commit()
cursor.execute("""UPDATE classicmodels.customers SET Country = "Norway" WHERE country = "Norway  ";""")
connection.commit()

for row in rows_customers:
    # récupérer le code du pays
    cursor.execute("""SELECT Code FROM world.country WHERE Name = %s;""", [row["country"]])
    country_code = cursor.fetchone()
    # country_code['Code']
    
    # récupérer la langue
    cursor.execute("""SELECT Language 
                   FROM world.countrylanguage 
                   WHERE IsOfficial = 'T' AND CountryCode = %s;""",
                   [country_code["Code"]])
    language = cursor.fetchone()
    # language["Language"]

    # trouver les codes iso qui correspondent aux langues
    # mettre le bon code dans une variable
    df = pd.read_csv("language-codes_csv.csv")
  
    if language["Language"] == 'Pilipino':
        l = 'fi'
    else:
        langue = df.loc[df["English"] == language["Language"], "alpha2"]
        l = langue.values[0]

    cursor.execute("""Insert into classicmodels2.customers(
                   customerNumber,
                   customerName,
                   contactLastName,
                   contactFirstName,
                   phone,
                   addressLine1,
                   addressLine2,
                   city,
                   state,
                   postalCode,
                   country,
                   salesRepEmployeeNumber,
                   creditLimit,
                   language
                   ) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);""", 
                   [row["customerNumber"], 
                    row["customerName"], 
                    row["contactLastName"], 
                    row["contactFirstName"], 
                    row["phone"], 
                    row["addressLine1"],
                    row["addressLine2"], 
                    row["city"], 
                    row["state"], 
                    row["postalCode"], 
                    country_code['Code'], 
                    row["salesRepEmployeeNumber"], 
                    row["creditLimit"],
                    l
                    ])
connection.commit()


#Insérer la table payments
cursor.execute("""SELECT * FROM classicmodels.payments;""")
rows_payments = cursor.fetchall()
for row in rows_payments:
    try:
        cursor.execute("""Insert into classicmodels2.payments(customerNumber,checkNumber,paymentDate,amount) values (%s, %s, %s, %s);""", [row["customerNumber"], row["checkNumber"], row["paymentDate"], row["amount"]])
    except:
        continue
connection.commit()

#Insérer la table orders
cursor.execute("""SELECT * FROM classicmodels.orders;""")
rows_orders = cursor.fetchall()
for row in rows_orders:
    try:
        cursor.execute("""Insert into classicmodels2.orders(orderNumber,orderDate,requiredDate,shippedDate,status,comments,customerNumber) values (%s,%s,%s,%s,%s,%s,%s);""", [row["orderNumber"], row["orderDate"], row["requiredDate"], row["shippedDate"], row["status"], row["comments"], row["customerNumber"]])
    except:
        continue
connection.commit()

#Insérer la table orderdetails
cursor.execute("""SELECT * FROM classicmodels.orderdetails;""")
rows_orderdetails = cursor.fetchall()
for row in rows_orderdetails:
    try:
        cursor.execute("""Insert into classicmodels2.orderdetails(orderNumber,productCode,quantityOrdered,priceEach,orderLineNumber) values (%s,%s,%s,%s,%s);""", [row["orderNumber"], row["productCode"], row["quantityOrdered"], row["priceEach"], row["orderLineNumber"]])
    except:
        continue
connection.commit()





