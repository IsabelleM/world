# Questions.

## 1.Pour chaque pays (avec au moins 1 client), le nombre de clients, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.
```SQL
SELECT customers.country, COUNT(customers.CustomerNumber) AS Nbreclients, Max(customers.language), 
SUM(priceEach * quantityOrdered) AS Chiffredaffaires FROM customers
JOIN orders ON orders.customerNumber = customers.customerNumber
JOIN orderdetails ON orderdetails.orderNumber = orders.orderNumber 
GROUP BY customers.country;
```
![Réponse question 21](Question21.png)

## 2. La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.
```SQL
SELECT customerName, SUM(priceEach * quantityOrdered) AS Chiffredaffaires FROM orderdetails 
JOIN orders ON orderdetails.orderNumber = orders.orderNumber 
JOIN customers ON orders.customerNumber = customers.customerNumber 
GROUP BY customerName ORDER BY Chiffredaffaires DESC
LIMIT 10;
```
![Réponse question 2](Question22.png)

## 3. La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).
```SQL
SELECT AVG(DATEDIFF(shippedDate, orderDate)) AS Dureemoyenne FROM orders;
```
![Réponse question 3](Question23.png)

## 4. Les 10 produits les plus vendus.
```SQL
SELECT productName, SUM(quantityOrdered) AS quantitevendue FROM orderdetails
JOIN products ON products.productCode = orderdetails.productCode GROUP BY productName 
ORDER BY quantitevendue DESC LIMIT 10;
```
![Réponse question 24](Question24.png)

## 5. Pour chaque pays, le produit le plus vendu.
```SQL
SELECT country, productName, SUM(quantityOrdered) AS quantitevendue FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
GROUP BY country, productName
ORDER BY quantitevendue DESC LIMIT 60;
```
J'ai tous les produits pour tous les pays mais pas le produit le plus vendu par pays.


## 6. Le produit qui a rapporté le plus de bénéfices.
```SQL
SELECT productName, SUM(priceEach * quantityOrdered) AS Benefices FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
GROUP BY productName ORDER BY Benefices DESC LIMIT 1;
```
![Réponse question 6](Question26.png)

## 7. La moyenne des différences entre le MSRP(prix de vente conseillé) et le prix de vente (en pourcentage).
```SQL
SELECT products.productName, AVG(MSRP - priceEach) AS Moyennedifferences FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode 
GROUP BY productName
ORDER BY Moyennedifferences DESC;
```
![Réponse question 7](Question27.png)

## 8. Pour chaque pays (avec au moins 1 client), le nombre de bureaux dans le même pays.
```SQL
SELECT offices.country, COUNT(*) AS Nbredebureaux FROM offices
GROUP BY country;
```
![Réponse question 28](Question28.png)
