use world;

SELECT name, Nbrelangues, NbrelanguesOfficielles
FROM (
    SELECT 
    country.Name as Name, 
    COUNT(Language) AS Nbrelangues,
    country.code as Code
    FROM country 
    JOIN countrylanguage ON country.Code = CountryCode
    GROUP BY Code
) as R1
JOIN (
    SELECT COUNT(r2.Language) AS NbrelanguesOfficielles,
    country.code as Code
    FROM country 
    JOIN countrylanguage AS r2 ON country.Code = r2.CountryCode
    where r2.IsOfficial = 'T'
    GROUP BY Code
) as R2 
ON R1.Code = R2.Code;


